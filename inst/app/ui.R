miniPage(
  gadgetTitleBar("Easy Data Explorer"),
  tags$head(
    tags$style(HTML("
                    .flexfill-item-inner {
                    display: flex;
                    flex-direction: column;

                    display: -webkit-flex;
                    -webkit-flex-direction: column;
                    }
                    .gadget-scroll {
                    min-width: 350px;
                    }
                    #table {
                    height: 100%;
                    }
                    #table .tab-content {
                    top: 50px;
                    }
                    #chain-chain {
                    display: flex;
                    flex-direction: column;

                    display: -webkit-flex;
                    -webkit-flex-direction: column;
                    }

                    #chain-chain div {
                    display: flex;
                    flex-direction: row;

                    display: -webkit-flex;
                    -webkit-flex-direction: row;
                    }

                    #chain-chain .expression-btn {
                    flex-grow: 1;
                    -webkit-flex-grow: 1;
                    padding: 1px;
                    }

                    #chain-chain .expression-btn pre {
                    width: 100%;
                    margin: 1px;
                    }
                    "))
  ),
  miniContentPanel(
    fillRow(flex = c(NA, 2),
            ProcessChainUI("chain"),
            miniTabstripPanel(
              miniTabPanel("Print", icon = icon("terminal"),
                           miniContentPanel(
                             verbatimTextOutput("print")
                           )
              ),
              miniTabPanel("Summary", icon = icon("table"),
                           miniContentPanel(
                             h4("Structure of Dataset"),
                             verbatimTextOutput("structure"),
                             h4("Summary of Dataset"),
                             verbatimTextOutput("summary")
                           )
              ),
              miniTabPanel("Data", icon = icon("table"),
                           miniContentPanel(
                             uiOutput("table")
                           )
              ),
              miniTabPanel("Visualize", icon = icon("area-chart"),
                           radiantVisualizeUI("visualize")
              )

            ))
  )

)
