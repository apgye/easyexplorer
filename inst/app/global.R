library(shiny)
library(miniUI)
library(shinyWidgets)
library(dplyr)
library(ggplot2)
library(shinyAce)

library(apgyeOperationsJusER)

library(EasyExplorer)
# source('ProcessChainGUI.R')
# source('process_chain.R')
# source('run_easy_explorer.R')
# source('RadiantVisualizeGUI.R')


displayTableData <- function (x, ...) {
  UseMethod("displayTableData", x)
}
displayTableData.tbl_lazy <- function(dataToDisplay, output, id) {
  tagList(
    tags$p("Displaying up to 100 items"),
    dataToDisplay %>% head(100) %>% collect() %>% as.data.frame() %>% displayTableData(output, id)
  )

}

displayTableData.data.frame <- function(dataToDisplay, output, id) {
  dataTableOut <- DT::datatable(dataToDisplay,
                                extensions = 'Scroller',
                                style = 'bootstrap',
                                fillContainer = TRUE,
                                filter = 'top',

                                options = list(
                                  dom = 't',
                                  autoWidth = TRUE,
                                  scroller = TRUE,
                                  class='table-bordered table-condensed'
                                )
  )
  output[[id]] <- DT::renderDataTable(dataTableOut, server = FALSE)
  DT::dataTableOutput(id, height = "100%")
}

displayTableData.list <- function(dataToDisplay, output, id) {
  items <- list()
  for(itemName in names(dataToDisplay)) {
    items[[itemName]] <- tabPanel(
      title = itemName,
      displayTableData(dataToDisplay[[itemName]], output, paste(id, itemName, sep='-'))
    )
  }

  do.call(tabsetPanel, c(id="tabs", type="pills", unname(items)))
}


